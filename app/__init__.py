name = "app"

import pygame, math
from pygame.locals import *
from .colors import clr
from .vectors import fakePVector2
from .datacollector import numofdots2goal
import hook

screensize = width, height = 500, 500
pygame.init()
screen = pygame.display.set_mode(screensize)
pygame.display.set_caption("DotMachineLearning")

bkgnd = pygame.Surface(screen.get_size())
bkgnd = bkgnd.convert()
bkgnd.fill(clr.white)

goalpos = dict(x=width / 2, y=height / 20)


def disp_txt(gen, pos, size):
	gen = str(gen)
	font = pygame.font.Font('freesansbold.ttf', size)
	TextSurf = font.render(gen, True, (0, 255, 0))
	TextRect = TextSurf.get_rect()
	TextRect.center = (pos[0], pos[1])
	bkgnd.blit(TextSurf, TextRect)


from .population import Population

test = Population(100)


def main():
	screen.blit(bkgnd, (0, 0))
	pygame.display.flip()

	while 1:
		# check for QUIT event
		for event in (event for event in pygame.event.get() if event.type == QUIT):
			return

		# Clear the Screen
		bkgnd.fill(clr.white)

		# Draw the Goal
		goal_rect = pygame.draw.circle(
			bkgnd,
			clr.red,
			fakePVector2(goalpos["x"], goalpos["y"]),
			4
		)

		if test.allDotsDead():
			# Genetic Algorithm
			test.calculatefitness()
			# Collecting Data
			if test.bestdot is not None:
				bdfitness = test.bestdot.fitness
			else: bdfitness = 0
			hook.Run("EndofGeneration",
			         gen=test.generation,
			         highestFitness=bdfitness,
			         fitnessSum=test.fitnesssum,
			         dots=test.dots
	         )
			test.naturalselection()
			test.mutatechildren()
		else:
			test.update()
			test.show()

		# Ensure Generation Counter is Up-to-Date
		disp_txt(test.generation, (20, 20), 20)
		# List Current HighScore
		if test.bestdot is not None:
			bestdotfitness = str(test.bestdot.fitness)
			bestdotfitness = bestdotfitness[:bestdotfitness.index(".") + 4]
			disp_txt("Highest", (30, 40), 15)
			disp_txt("Fitness: {}".format(bestdotfitness), (50, 55), 15)

		disp_txt("Directions: {}".format(test.maxdirections), (60, 70), 15)

		screen.blit(bkgnd, (0, 0))

		# Update Display
		pygame.display.flip()


def start(): main()
