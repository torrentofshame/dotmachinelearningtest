from .ocround import r2h
import math


class ParamError(Exception):
    """
	Exception raised for errors in function parameters.

    Attributes:
        funcname -- the name of the function in which the error occurred
        message -- explanation of the error
    """

    def __init__(self):
        self.message = "Improper Input"


def fakePVector2(x, y):
    """
	Cause Python won't fucking let me return a list instead of the object
	:param x:
	:param y:
	:returns: list in [x,y] format
	"""
    check_ls = [x, y, float(x), float(y)]
    # basically runs all the shit in check_ls through an if statement without weird ass nested fuckery
    if len(check_ls) != len([c for c in check_ls if c]):
        raise ParamError()
    return [r2h(x), r2h(y)]


def PVector2_fromAngle(angle):
    """
	Creates a PVector2 using an angle
	:param angle:
	:returns: PVector2
	"""
    return PVector2(math.cos(angle), math.sin(angle))


class PVector2:
    """
	Cause pygame vectors are shit and don't work well.
	
	Attributes:
		x -- the x-value
		y -- the y-value
		value -- [x, y]
		add -- adding vectors
		sub -- subtracting vectors
		magnitude -- the magnitude
	"""

    def __init__(self, x, y):
        check_ls = [x, y, float(x), float(y)]
        # basically runs all the shit in check_ls through an if statement without weird ass nested fuckery
        if (len(check_ls) != len([c for c in check_ls if c])) and (x != 0 and y != 0):
            raise ParamError
        self.value = [self.x, self.y] = [x, y]  # [r2h(x), r2h(y)]

    def add(self, v):
        self.value = [self.x, self.y] = [self.x + v.x, self.y + v.y]

    def sub(self, v):
        self.value = [self.x, self.y] = [self.x - v.x, self.y - v.y]

    def magnitude(self):
        mag = math.sqrt(math.pow(self.x, 2) + math.pow(self.y, 2))
        return mag
