from . import *
from .dot import Dot
from .colors import clr
import pygame
from numpy import random
import copy


class Population:

	def __init__(self, size: int):
		self.maxdirections = 10
		self.generation = 0
		self.dots = []
		self.fitnesssum = 0
		self.bestdot = None
		self.increasemoves = True

		for i in range(size):
			self.dots.append(Dot(self.maxdirections))

	def show(self):
		for dot in self.dots:
			dot.show()

	def update(self):
		for dot in self.dots:
			dot.update()

	def calculatefitness(self):
		for d in self.dots:
			d.calculatefitness()

	def allDotsDead(self):
		for d in self.dots:
			if d.isalive and not d.reachedgoal:
				return False
		return True

	def naturalselection(self):
		self.nextgeneration()
		newdots = []
		self.calculatefitness()
		self.findBestDot()
		bestdotchild = None
		if self.bestdot is not None:
			bestdotchild = self.bestdot.getchild(self.maxdirections, True)
			for i in range(len(self.dots) // 4):  # 1/4 come directly from the prev gen best dot
				newdots.append(self.bestdot.getchild(self.maxdirections))
		for i in range(len(self.dots) if self.bestdot is None else (len(self.dots)*3) // 4):  # 3/4 come from parents selected normally
			parent = self.selectparent()
			newdots.append(parent.getchild(self.maxdirections))
		if bestdotchild is not None:
			newdots.append(bestdotchild)
		self.dots = newdots.copy()

	def calculatefitnessSum(self):
		for d in self.dots:
			self.fitnesssum += d.fitness

	def selectparent(self):
		rand = random.uniform(0, self.fitnesssum)

		runningsum = 0
		for i in range(len(self.dots)):
			runningsum += self.dots[i].fitness
			if runningsum > rand:
				return self.dots[i]

	def mutatechildren(self):
		for i in range(1, len(self.dots)):
			self.dots[i].mutatebrain()

	def findBestDot(self):
		best = None
		for d in self.dots:
			if best is None:
				best = d
			elif best.fitness < d.fitness:
				best = d
		self.bestdot = best

	def nextgeneration(self):
		for d in self.dots:
			if d.reachedgoal:
				self.increasemoves = False
		self.generation += 1
		if self.increasemoves:
			if self.generation > 0 and self.generation % 5 == 0:
				self.maxdirections += 10
