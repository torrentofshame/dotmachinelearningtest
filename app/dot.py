from . import *
from pygame import draw
from .colors import clr
from .vectors import PVector2, PVector2_fromAngle
from math import tau
from .ocround import r2h
from numpy import random
import copy


class Dot:

	def __init__(self, maxdirections, bestboomer=False):

		self.maxdirections = maxdirections
		self.bestboomer = bestboomer
		self.isalive = True
		self.reachedgoal = False
		self.rect = None
		# Brain Shit
		self.directions = []
		self.step = 0
		# fitness
		self.fitness = 0

		randAngles = random.uniform(0, tau, size=(1, self.maxdirections))
		for a in randAngles[0]:
			self.directions.append(PVector2_fromAngle(a))

		self.p = PVector2(width / 2, height * 0.9)
		self.v = PVector2(0, 0)
		self.a = PVector2(0, 0)

	def show(self):
		if self.bestboomer:
			dotclr = clr.green
		else:
			dotclr = clr.black
		self.rect = draw.circle(
			bkgnd,
			dotclr,
			[r2h(self.p.x), r2h(self.p.y)],
			4
		)

	def move(self):
		if len(self.directions) > self.step:
			self.a = self.directions[self.step]
			self.step += 1
		else:
			self.isalive = False

		self.v.add(self.a)
		if self.v.magnitude() > 5:  # Make sure we don't go too fast
			fact2five = 5 / max([abs(self.v.x), abs(self.v.y)])
			self.v = PVector2(self.v.x * fact2five, self.v.y * fact2five)
		self.p.add(self.v)

	def dist2goal(self):
		return math.sqrt(math.pow(goalpos["x"] - self.p.x, 2) + math.pow(goalpos["y"] - self.p.y, 2))

	def update(self):
		if self.isalive and not self.reachedgoal:
			self.move()
			if self.p.x < 2 or self.p.y < 2 or self.p.x > width - 2 or self.p.y > height - 2:
				self.isalive = False
			elif self.dist2goal() <= 4:
				self.reachedgoal = True
				hook.Run("DotReachedGoal")

	def calculatefitness(self):
		if self.reachedgoal:
			self.fitness = (1.0 / 16.0) + (10000.0 / math.pow(self.step, 2))
		else:
			self.fitness = 1.0 / math.pow(self.dist2goal(), 2)

	def getchild(self, maxdirs=0, bestboomer=False):
		if maxdirs == 0:
			maxdirs = self.maxdirections
		if bestboomer:
			child = Dot(maxdirections=maxdirs, bestboomer=True)
			child.directions = copy.deepcopy(self.directions)
		else:
			child = Dot(maxdirs)
			if len(child.directions) > len(self.directions):
				newdirs = child.directions[-5:]
				child.directions = copy.deepcopy(self.directions)
				child.directions.extend(newdirs)
			else:
				child.directions = copy.deepcopy(self.directions)
		return child

	def mutatebrain(self):
		mutationrate = 0.01
		if not self.bestboomer:
			for i in range(len(self.directions)):
				rand = random.uniform(0, 1)
				if rand < mutationrate:
					randAngle = random.uniform(0, tau)
					randVect = PVector2_fromAngle(randAngle)
					newx = (self.directions[i].x + randVect.x) / 2
					newy = (self.directions[i].y + randVect.y) / 2
					self.directions[i] = PVector2(newx, newy)
