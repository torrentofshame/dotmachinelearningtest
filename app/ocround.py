def r2h(n):
    """
	Probably OverComplicated Rounding to whole number
	:param n: the number to round
	:returns: rounded number as int
	"""
    if isinstance(n, int): return n;

    n_ls = list(str(n))
    wn = int("".join(n_ls[:n_ls.index(".")]))
    d_ls = n_ls[n_ls.index(".") + 1:]

    for d in d_ls:
        dn = int(d)

        if dn <= 3:
            break;  # Here there is no chance to round up
        elif dn == 4:
            continue;  # Here it depends if this value is rounded up

        # Here we round up
        wn += 1
        break

    return wn
