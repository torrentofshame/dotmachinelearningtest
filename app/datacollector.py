import hook, datetime, os, re, csv

numofdots2goal = 0


@hook.Add("DotReachedGoal")
def DotReachedGoal():
	global numofdots2goal
	numofdots2goal += 1


@hook.Add("EndofGeneration")
def EndofGeneration(gen, highestFitness, fitnessSum, dots):
	global numofdots2goal
	currentlog = 0
	for _, _, files in os.walk("./logs"):
		for filename in files:
			if re.search("^log\d+\.csv$", filename) and currentlog < int(filename[3:4]):
				currentlog = int(filename[3:4])

	if gen == 0:
		currentlog += 1
		with open("./app/logs/log{}.csv".format(currentlog), mode="w") as csv_file:
			fieldnames = ["timestamp", "generation", "highestFitness", "fitnessSum", "NumofDots2Goal", "dots"]

			writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

			writer.writeheader()
			writer.writerow({
				"timestamp": str(datetime.datetime.today()).replace(" ", ";"),
				"generation": gen,
				"highestFitness": highestFitness,
				"fitnessSum": fitnessSum,
				"NumofDots2Goal": numofdots2goal,
				"dots": dots
			})
			csv_file.close()
	else:
		if currentlog == 0: currentlog = 1;
		with open("./app/logs/log{}.csv".format(currentlog), mode="a") as csv_file:

			fieldnames = ["timestamp", "generation", "highestFitness", "fitnessSum", "NumofDots2Goal", "dots"]

			writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

			writer.writerow({
				"timestamp": str(datetime.datetime.today()).replace(" ", ";"),
				"generation": gen,
				"highestFitness": highestFitness,
				"fitnessSum": fitnessSum,
				"NumofDots2Goal": numofdots2goal,
				"dots": dots
			})
			csv_file.close()
	print("{} Dots reached the Goal!".format(numofdots2goal))
	numofdots2goal = 0
